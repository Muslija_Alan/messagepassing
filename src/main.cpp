#include <HelloPrinter.h>
#include <WorldPrinter.h>
#include <CommHandle.hpp>
#include <UnknownHandleError.hpp>

#include <iostream>
#include <thread>

using namespace MessagePassing;

int main() {
  WorldPrinter worldPrinter;
  HelloPrinter helloPrinter;

  try {
    std::thread t1{[&worldPrinter](const CommHandle& destination) {
                     worldPrinter.print(destination);
                   },
                   helloPrinter.commHandle()};
    std::thread t2{[&helloPrinter](const CommHandle& destination) {
                     helloPrinter.print(destination);
                   },
                   worldPrinter.commHandle()};

    t1.join();
    t2.join();
  } catch (const UnknownHandleError& err) {
    std::cout << err.what() << std::endl;
  }
  return 0;
}
