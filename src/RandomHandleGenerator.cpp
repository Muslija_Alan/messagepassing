#include <RandomHandleGenerator.h>
#include <CommHandle.hpp>

#include <cstdlib>
#include <limits>

namespace MessagePassing {

RandomHandleGenerator::RandomHandleGenerator()
    : maxValue_{std::numeric_limits<int>::max()} {}

CommHandle RandomHandleGenerator::commHandle() const {
  return CommHandle{rand() % maxValue_};
}
} /* MessagePassing */
