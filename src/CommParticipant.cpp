#include <CommParticipant.h>
#include <CommHandle.hpp>
#include <UnknownDestinationError.hpp>
#include <UnknownHandleError.hpp>

#include <algorithm>
#include <thread>

namespace MessagePassing {

std::unordered_map<CommHandle, CommParticipant*> CommParticipant::commObjects_;

RandomHandleGenerator CommParticipant::randomHandleGenerator_;

CommParticipant::CommParticipant() {
  commObjects_[randomHandleGenerator_.commHandle()] = this;
}

CommParticipant::~CommParticipant() = default;

void CommParticipant::blockingSend(const CommHandle& destination,
                                   const std::string& message) {
  auto it = commObjects_.find(destination);
  if (it != std::end(commObjects_)) {
    auto commObjectPtr = it->second;
    {
      std::lock_guard<std::mutex> lock{commObjectPtr->mtx_};
      commObjectPtr->msgQueue_.push(message);
    }
    commObjectPtr->condVar_.notify_one();
  } else
    throw UnknownDestinationError{"Cannot find destination!"};
}

void CommParticipant::send(const CommHandle& destination,
                           const std::string& message) {
  std::thread t{[&destination, message, this]() {
    this->blockingSend(destination, std::move(message));
  }};
  t.detach();
}

std::string CommParticipant::blockingReceive() {
  std::unique_lock<std::mutex> lock{mtx_};
  condVar_.wait(lock, [this]() { return !(this->msgQueue_.empty()); });
  auto msg = msgQueue_.front();
  msgQueue_.pop();
  return msg;
}

CommHandle CommParticipant::commHandle() const {
  auto it =
      std::find_if(std::cbegin(commObjects_), std::cend(commObjects_),
                   [this](const auto& pair) { return this == pair.second; });
  if (it != std::end(commObjects_)) return it->first;
  throw UnknownHandleError{"Cannot find handle!"};
}

} /* MessagePassing */
