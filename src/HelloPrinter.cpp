#include <HelloPrinter.h>
#include <CommHandle.hpp>
#include <UnknownDestinationError.hpp>

#include <iostream>

namespace MessagePassing {
void HelloPrinter::print(const CommHandle& destination) {
  try {
    while (true) {
      std::cout << blockingReceive();
      send(destination, "World!");
    }
  } catch (const UnknownDestinationError& err) {
    std::cout << err.what() << std::endl;
  }
}

} /* MessagePassing */
