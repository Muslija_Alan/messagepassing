#include <WorldPrinter.h>
#include <CommHandle.hpp>
#include <UnknownDestinationError.hpp>

#include <chrono>
#include <iostream>
#include <string>
#include <thread>

namespace MessagePassing {
void WorldPrinter::print(const CommHandle& destination) {
  try {
    while (true) {
      send(destination, "Hello ");
      std::cout << blockingReceive() << std::endl;
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
  } catch (const UnknownDestinationError& err) {
    std::cout << err.what() << std::endl;
  }
}
} /* MessagePassing  */
