#ifndef MESSAGE_PASSING_COMM_PARTICIPANT_H_
#define MESSAGE_PASSING_COMM_PARTICIPANT_H_
#include <RandomHandleGenerator.h>

#include <condition_variable>
#include <mutex>
#include <queue>
#include <string>
#include <unordered_map>

namespace MessagePassing {

struct CommHandle;

class CommParticipant {
  public:
  CommParticipant();
  void blockingSend(const CommHandle& destination, const std::string& message);
  void send(const CommHandle& destination, const std::string& message);
  std::string blockingReceive();
  CommHandle commHandle() const;
  virtual ~CommParticipant();

  private:
  static std::unordered_map<CommHandle, CommParticipant*> commObjects_;
  static RandomHandleGenerator randomHandleGenerator_;

  std::mutex mtx_;
  std::condition_variable condVar_;
  std::queue<std::string> msgQueue_;
};

} /* MessagePassing */
#endif /* ifndef MESSAGE_PASSING_COMM_PARTICIPANT_H_ */
