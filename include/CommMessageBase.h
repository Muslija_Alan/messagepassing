#ifndef MESSAGE_PASSING_COMM_MESSAGE_BASE_H_
#define MESSAGE_PASSING_COMM_MESSAGE_BASE_H_
#include <CommHandle.h>

namespace MessagePassing {
struct CommMessageBase {
  CommHandle handle;
};

} /* MessagePassing */
#endif /* ifndef MESSAGE_PASSING_COMM_MESSAGE_BASE_H_ */
