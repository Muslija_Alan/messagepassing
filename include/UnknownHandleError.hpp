#ifndef MESSAGE_PASSING_UNKNOWN_HANDLE_ERROR_HPP_
#define MESSAGE_PASSING_UNKNOWN_HANDLE_ERROR_HPP_
#include <stdexcept>

namespace MessagePassing {
class UnknownHandleError : public std::runtime_error {
  using runtime_error::runtime_error;
};
} /* MessagePassing */
#endif /* ifndef MESSAGE_PASSING_UNKNOWN_HANDLE_ERROR_HPP_ */
