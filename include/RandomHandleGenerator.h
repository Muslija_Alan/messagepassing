#ifndef MESSAGE_PASSING_RANDOM_HANDLE_GENERATOR_H_
#define MESSAGE_PASSING_RANDOM_HANDLE_GENERATOR_H_

namespace MessagePassing {

struct CommHandle;

class RandomHandleGenerator {
  public:
  RandomHandleGenerator();
  CommHandle commHandle() const;

  private:
  int maxValue_;
};
} /* MessagePassing  */
#endif /* ifndef MESSAGE_PASSING_RANDOM_HANDLE_GENERATOR_H_ */
