#ifndef MESSAGE_PASSING_COMM_HANDLE_H_
#define MESSAGE_PASSING_COMM_HANDLE_H_
#include <functional>

namespace MessagePassing {

struct CommHandle {
  int id;
};

inline bool operator==(const CommHandle& lhs, const CommHandle& rhs) {
  return lhs.id == rhs.id;
}

} /* MessagePassing  */

namespace std {
template <>
struct hash<MessagePassing::CommHandle> {
  std::size_t operator()(const MessagePassing::CommHandle& k) const {
    return hash<int>()(k.id);
  }
};

} /* std */
#endif /* ifndef MESSAGE_PASSING_COMM_HANDLE_H_ */
