#ifndef MESSAGE_PASSING_WORLD_PRINTER_H_
#define MESSAGE_PASSING_WORLD_PRINTER_H_
#include <CommParticipant.h>

namespace MessagePassing {

struct CommHandle;

class WorldPrinter : public CommParticipant {
  public:
  void print(const CommHandle& destination);
};

} /* MessagePassing  */
#endif /* ifndef MESSAGE_PASSING_WORLD_PRINTER_H_ */
