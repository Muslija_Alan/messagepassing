#ifndef MESSAGE_PASSING_HELLO_PRINTER_H_
#define MESSAGE_PASSING_HELLO_PRINTER_H_
#include <CommParticipant.h>

namespace MessagePassing {

struct CommHandle;

class HelloPrinter : public CommParticipant {
  public:
  void print(const CommHandle& destination);
};
} /* MessagePassing  */
#endif /* ifndef MESSAGE_PASSING_HELLO_PRINTER_H_ */
